/*1. How do you create arrays in JS?
2. How do you access the first character of an array?
3. How do you access the last character of an array?
4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
6. What array method creates a new array with elements obtained from a user-defined function?|
7. What array method checks if all its elements satisfy a given condition?
8. What array method checks if at least one of its elements satisfies a given condition?
9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
10. True or False: array.slice() copies elements from original array and returns them as a new array.

// Answer:
/*
   1. using square brackets []
   2. We use index 0 to access the first character
   3. using the index array.length - 1
   4. indexOf()
   5. forEach()
   6. map()
   7. every()
   8. some()
   9. false
   10. true
*/

// Function Coding
let students = ["John", "Joe", "Jane", "Jessie"];

function addToEnd(arr, element) {
    if (typeof element !== "string") {
        return "error - can only add strings to an array";
    }
    arr.push(element);
    return arr;
}

function addToStart(arr, element) {
    if (typeof element !== "string") {
        return "error - can only add strings to an array";
    }
    arr.unshift(element);
    return arr;
}

function elementChecker(arr, value) {
    if (arr.length === 0) {
        return "error - passed in array is empty";
    }
    return arr.includes(value);
}

function checkAllStringsEnding(arr, character) {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }
    for (let element of arr) {
        if (typeof element !== "string") {
            return "error - all array elements must be strings";
        }
    }
    if (typeof character !== "string") {
        return "error - 2nd argument must be of data type string";
    }
    if (character.length !== 1) {
        return "error - 2nd argument must be a single character";
    }
    return arr.every(element => element.endsWith(character));
}

function stringLengthSorter(arr) {
    for (let element of arr) {
        if (typeof element !== "string") {
            return "error - all array elements must be strings";
        }
    }
    return arr.slice().sort((a, b) => a.length - b.length);
}

function startsWithCounter(arr, character) {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }
    for (let element of arr) {
        if (typeof element !== "string") {
            return "error - all array elements must be strings";
        }
    }
    if (typeof character !== "string") {
        return "error - 2nd argument must be of data type string";
    }
    if (character.length !== 1) {
        return "error - 2nd argument must be a single character";
    }
    character = character.toLowerCase();
    return arr.reduce((count, element) => element.toLowerCase().startsWith(character) ? count + 1 : count, 0);
}

function likeFinder(arr, search_string) {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }
    for (let element of arr) {
        if (typeof element !== "string") {
            return "error - all array elements must be strings";
        }
    }
    if (typeof search_string !== "string") {
        return "error - 2nd argument must be of data type string";
    }
    search_string = search_string.toLowerCase();
    return arr.filter(element => element.toLowerCase().includes(search_string));
}

function randomPicker(arr) {
    if (arr.length === 0) {
        return "error - array must NOT be empty";
    }
    return arr[Math.floor(Math.random() * arr.length)];
}

